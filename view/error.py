from error.exception import UnauthorizedException, PathDoesNotExistException, PathContainsNothingException, PathIsEmptyException
from flask import redirect, url_for, flash, Blueprint


error = Blueprint('error', __name__, template_folder='templates')


@error.app_errorhandler(401)
@error.app_errorhandler(405)
@error.app_errorhandler(UnauthorizedException)
def unauthorized_error(error):
    flash("Error: {}".format(error))
    return redirect(location=url_for("home.index"))


@error.app_errorhandler(PathDoesNotExistException)
@error.app_errorhandler(PathContainsNothingException)
@error.app_errorhandler(PathIsEmptyException)
def path_does_not_exist_error(error):
    flash("Error: {}".format(error))
    return redirect(location=url_for("home.index"))


@error.app_errorhandler(404)
def invalid_route_error(error):
    flash("Error: Invalid URL.")
    return redirect(location=url_for("home.index"))
