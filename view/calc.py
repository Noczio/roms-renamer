from flask import redirect, url_for, session, Blueprint
from error.exception import UnauthorizedException

from model.files import rename_files_in_directory
from model.map import map_options


calc = Blueprint('calc', __name__, template_folder='templates')


@calc.route("/compute", methods=["POST"])
def compute():
    if session.get("User_variables"):
        folder_path = session["User_variables"]["Folder_path"]
        options = session["User_variables"]["Options"]
        delete_conditions = map_options(options)
        messages = rename_files_in_directory(folder_path, *delete_conditions)
        
        session["Messages"] = messages

        return redirect(location=url_for("final.result"))

    raise UnauthorizedException()
