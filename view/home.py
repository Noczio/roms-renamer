from flask import redirect, url_for, render_template, request, session, Blueprint, get_flashed_messages
from model.path import PathForm


home = Blueprint('home', __name__, template_folder='templates')


@home.route("/", methods=["GET", "POST"])
def index():
    path_form = PathForm()
    flashed_messages = get_flashed_messages()
    session.clear()

    if request.method == "GET":
        return render_template("home.html", form=path_form, flashed_messages=flashed_messages)

    path_form.validate()
    session["User_variables"] = {
        "Folder_path": path_form.folder_path.data,
        "Options": path_form.options.data
    }

    return redirect(location=url_for("calc.compute"), code=307)
