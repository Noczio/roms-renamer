class UnauthorizedException(Exception):
    pass


class PathDoesNotExistException(Exception):
    pass


class PathContainsNothingException(Exception):
    pass


class PathIsEmptyException(Exception):
    pass
