# ROMs renamer

Have you ever wonder how to rename all of your games in 1 click to a standard naming conventions?, no unnecessary trailing numbers in gba and nds, regions in their full name and a option to delete languages or version/revision to have a clean playlist. Well ... this repo is desktop app/webpage made in Python to rename ROMs, opensource and compatible with Windows.

## Instructions

- Download the .exe from the releases or here: https://drive.google.com/drive/folders/17Zw68n36UMTgKrRGBO0SOYUIQnZBx5u3?usp=sharing.
- Execute the app.

If by any chance you prefer to compile it by yourself, follow these steps inside a terminal with Ananconda/Miniconda and git installed:

Clone the repo
> git clone https://gitlab.com/Noczio/roms-renamer.git

Get inside the folder
> cd .\roms-renamer\

Create a conda env
> conda env create -f environment.yml

Activate the conda env
> conda activate ROMs-renamer

Compile the software
> pyinstaller -n ROMs-renamer -w --add-data "templates;templates" --add-data "static;static" --add-data "start;start" --add-data "model;model" --add-data "error;error" --add-data "view;view" -i app.ico -F app.py

## Notes

Games like "007 - From Russia with Love (Europe)" which starts with numbers and then a hyphen shouldn't be used with the delete trailing numbers options, otherwise it'll lose that important part of the name. Also, this software always changes the region to its full name convention, ex: U -> USA, UJ -> USA, Japan.

## Important

Because this was compiled with pyinstaller Windows Defender detects it as a virus, so it's advised to add the folder where the .exe is to an exclusion list. Yes, even if you decided it to compile it yourself. Although this never happened with other antiviruses like BitDefender.