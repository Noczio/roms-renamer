import unittest
from model.clean import ROMCleaner 


class TestCleanRomName(unittest.TestCase):
    def test_clean_rom_name_case_1(self):
        test_cases = [
            ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, It) (Nonsense).gba",
             "Mega Man Battle Network 6 - Cybeast Gregar (Europe) (Es, En, It).gba"),
            ("5678 - Pokemon - Ruby Version (U) (Es En It Jp)(Independent) (v1)[non-important].gba",
             "Pokemon - Ruby Version (USA) (Es En It Jp).gba"),
            ("9999 - Final Fantasy VII - Remake (J)(Rev 3)(what is even this)(Hack).chd",
             "Final Fantasy VII - Remake (Japan).chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (V2)(U)(Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (USA).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (v 3.6)(UJS)(Hack) [b].iso",
             "abcde - Some Game - With - Multiple - Dashes (USA, Japan, Spain).iso")
        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                self.assertEqual(cleaner.clean_rom(input_name, True, False, True, True), expected_output)

    def test_clean_rom_name_case_2(self):
        test_cases = [
            ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, It) (Nonsense).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (Europe).gba"),
            ("5678 - Pokemon - Ruby Version (U) (Es En It Jp)(Independent) (v1).gba",
             "5678 - Pokemon - Ruby Version (USA) (v1).gba"),
            ("9999 - Final Fantasy VII - Remake (J)(Rev 3)(Hack).chd",
             "9999 - Final Fantasy VII - Remake (Japan)(Rev 3).chd"),
             ("9999 - Final Fantasy VII - Remake (J)(Rev.3)(Hack).chd",
             "9999 - Final Fantasy VII - Remake (Japan)(Rev.3).chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (V2)(U)(Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (V2)(USA).nds"),
             ("ABCDE - Some Game - With - Multiple - Dashes (V.2.0)(U)(Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (V.2.0)(USA).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (v 3.6)(UJS)(Hack).iso",
             "abcde - Some Game - With - Multiple - Dashes (v 3.6)(USA, Japan, Spain).iso"),
            ("neo9999 - Final Fantasy VII - Remake (JU) (Jp, En, Es)(Rev 3)(Hack) (!b).chd",
             "neo9999 - Final Fantasy VII - Remake (Japan, USA) (Rev 3).chd"),
        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                self.assertEqual(cleaner.clean_rom(input_name, False, True, False, True), expected_output)
        

    def test_clean_rom_name_case_3(self):
        test_cases = [
            ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, It) (Nonsense).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (Europe) (Nonsense).gba"),
            ("5678 - Pokemon - Ruby Version (U) (Es En It Jp)(Independent) (v1).gba",
             "5678 - Pokemon - Ruby Version (USA) (Independent) (v1).gba"),
            ("9999 - Final Fantasy VII - Remake (J)(Rev 3) [*].chd",
             "9999 - Final Fantasy VII - Remake (Japan)(Rev 3) [*].chd"),
             ("9999 - Final Fantasy VII - Remake (J)(Rev.3)[!unimportant].chd",
             "9999 - Final Fantasy VII - Remake (Japan)(Rev.3)[!unimportant].chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (V2)(U)[!important].nds",
             "ABCDE - Some Game - With - Multiple - Dashes (V2)(USA)[!important].nds"),
             ("ABCDE - Some Game - With - Multiple - Dashes (V.2.0)(U)(Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (V.2.0)(USA)(Hack).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (v 3.6)(UJS)(Hack).iso",
             "abcde - Some Game - With - Multiple - Dashes (v 3.6)(USA, Japan, Spain)(Hack).iso"),
            ("neo9999 - Final Fantasy VII - Remake (JU) (Jp, En, Es)(Rev 3)(Hack) (!b).chd",
             "neo9999 - Final Fantasy VII - Remake (Japan, USA) (Rev 3)(Hack) (!b).chd"),
        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                self.assertEqual(cleaner.clean_rom(input_name, False, True, False, False), expected_output)


    def test_clean_trailing_numbers(self):
        test_cases = [
            ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E)(Nonsense).gba",
             "Mega Man Battle Network 6 - Cybeast Gregar (E)(Nonsense).gba"),
            ("5678 - Pokemon - Ruby Version (U)(Independent).gba",
             "Pokemon - Ruby Version (U)(Independent).gba"),
            ("9999 - Final Fantasy VII - Remake (J)(Hack).chd",
             "Final Fantasy VII - Remake (J)(Hack).chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (U)(Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (U)(Hack).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (U)(Hack).iso",
             "abcde - Some Game - With - Multiple - Dashes (U)(Hack).iso")
        ]

        cleaner = ROMCleaner()

        for input_name, expected_output in test_cases:
            with self.subTest():
                self.assertEqual(cleaner.clean_trailing_numbers(
                    input_name), expected_output)

    def test_clean_languages(self):
        test_cases = [
            (" 1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense) .gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Nonsense).gba"),
            (" 5678 - Pokemon - Ruby Version (U) (Es En Jp) (Independent).gba",
             "5678 - Pokemon - Ruby Version (U) (Independent).gba"),
            ("9999 - Final Fantasy VII - Remake (J)(Hack).chd",
             "9999 - Final Fantasy VII - Remake (J)(Hack).chd"),
            (" ABCDE - Some Game - With - Multiple - Dashes (U)  (Es, En, Jp)(Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (U) (Hack).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (U)(Es En Jp)(Hack).iso",
             "abcde - Some Game - With - Multiple - Dashes (U)(Hack).iso")
        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                no_languages = cleaner.clean_languages(input_name)
                result = cleaner.remove_extra_spaces(no_languages)
                self.assertEqual(result, expected_output)

    def test_clean_version_case_1(self):
        test_cases = [
            ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense) (v1.0).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense).gba"),
            (" 5678 - Pokemon - Ruby Version (U) (Es En Jp) (REV 3) (Independent).gba",
             "5678 - Pokemon - Ruby Version (U) (Es En Jp) (Independent).gba"),
            ("9999 - Final Fantasy VII - Remake (J)(Hack) (V 2.1).chd",
             "9999 - Final Fantasy VII - Remake (J)(Hack).chd"),
            (" ABCDE - Some Game - With - Multiple - Dashes (U) (Es, En, Jp)(Hack) .nds",
             "ABCDE - Some Game - With - Multiple - Dashes (U) (Es, En, Jp)(Hack).nds"),
            (" abcde - Some Game - With - Multiple - Dashes (U)(Hack)(v 3.4) .iso",
             "abcde - Some Game - With - Multiple - Dashes (U)(Hack).iso"),
            (" abcde - Some Game - With - Multiple - Dashes (U)(Hack) (rev. 2.1) .iso",
             "abcde - Some Game - With - Multiple - Dashes (U)(Hack).iso"),
             (" abcde - Some Game - With - Multiple - Dashes (U)(Hack) (rev.2.1) .iso",
             "abcde - Some Game - With - Multiple - Dashes (U)(Hack).iso"),
             ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense) (Rev. 1.0).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense).gba"),
             ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense) (Rev.3).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense).gba"),
             ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense) (Rev.2.0).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp) (Nonsense).gba"),
        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                no_version = cleaner.clean_version(input_name)
                result = cleaner.remove_extra_spaces(no_version)
                self.assertEqual(result, expected_output)
    
    
    def test_remove_unwated_text(self):
        test_cases = [
            ("1234 - Mega Man Battle Network 6 - Cybeast Gregar (Europe)(Nonsense).gba",
             "1234 - Mega Man Battle Network 6 - Cybeast Gregar (Europe).gba"),
            ("Pokemon - Ruby Version (USA)(Independent) (!n).gba",
             "Pokemon - Ruby Version (USA).gba"),
            ("Ano9999 - Final Fantasy VII - Remake (Japan, Spain)(Hack).chd",
             "Ano9999 - Final Fantasy VII - Remake (Japan, Spain).chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (USA) (En, Es, Jp, Fr) (Hack).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (USA) (En, Es, Jp, Fr).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (USA)(En Es Jp Fr) (Hack) (!MoreNonsense).iso",
             "abcde - Some Game - With - Multiple - Dashes (USA)(En Es Jp Fr).iso")
        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                cleaned = cleaner.remove_extra_info(input_name)
                result = cleaner.remove_extra_spaces(cleaned)
                self.assertEqual(result, expected_output)

    def test_replace_region_name_simple(self):
        test_cases = [
            ("Mega Man Battle Network 6 - Cybeast Gregar (E) (Es, En, Jp).gba",
             "Mega Man Battle Network 6 - Cybeast Gregar (Europe) (Es, En, Jp).gba"),
            ("Pokemon - Ruby Version (U).gba",
             "Pokemon - Ruby Version (USA).gba"),
            ("Final Fantasy VII - Remake (J).chd",
             "Final Fantasy VII - Remake (Japan).chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (USA) (Es En Jp).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (USA) (Es En Jp).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (S).iso",
             "abcde - Some Game - With - Multiple - Dashes (Spain).iso"),
            ("abcde - Some Game - With - Multiple - Dashes (K).iso",
             "abcde - Some Game - With - Multiple - Dashes (Korea).iso"),
             ("abcde - Some Game - With - Multiple - Dashes.iso",
             "abcde - Some Game - With - Multiple - Dashes.iso")

        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                self.assertEqual(cleaner.clean_region(
                    input_name), expected_output)

    def test_replace_region_name_combination(self):
        test_cases = [
            ("Mega Man Battle Network 6 - Cybeast Gregar (EU) (Es, En, It).gba",
             "Mega Man Battle Network 6 - Cybeast Gregar (Europe, USA) (Es, En, It).gba"),
            ("Pokemon - Ruby Version (UJ) (Es, En, Jp).gba",
             "Pokemon - Ruby Version (USA, Japan) (Es, En, Jp).gba"),
            ("Final Fantasy VII - Remake (JTS).chd",
             "Final Fantasy VII - Remake (Japan, Taiwan, Spain).chd"),
            ("ABCDE - Some Game - With - Multiple - Dashes (USA) (Es En It).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (USA) (Es En It).nds"),
             ("ABCDE - Some Game - With - Multiple - Dashes (Es En It).nds",
             "ABCDE - Some Game - With - Multiple - Dashes (Es En It).nds"),
            ("abcde - Some Game - With - Multiple - Dashes (ST).iso",
             "abcde - Some Game - With - Multiple - Dashes (Spain, Taiwan).iso"),
            ("abcde - Some Game - With - Multiple - Dashes (KTJ).iso",
             "abcde - Some Game - With - Multiple - Dashes (Korea, Taiwan, Japan).iso")

        ]

        for input_name, expected_output in test_cases:
            with self.subTest():
                cleaner = ROMCleaner()
                self.assertEqual(cleaner.clean_region(
                    input_name), expected_output)


if __name__ == '__main__':
    unittest.main()
