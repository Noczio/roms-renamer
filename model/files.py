import os
from model.clean import ROMCleaner


def rename_files_in_directory(directory: str, delete_trailing_numbers: bool, delete_languages: bool, delete_version: bool) -> list[str]:
    messages = []
    cleaner = ROMCleaner()

    for filename in os.listdir(directory):

        filepath = os.path.join(directory, filename)

        if os.path.isfile(filepath):
            # Extracting file extension
            file_name, file_extension = os.path.splitext(filename)

            cleaned_name = cleaner.clean_rom(
                file_name, delete_trailing_numbers, delete_languages, delete_version)

            # New file name
            new_filename = cleaned_name + file_extension

            # Renaming the file
            new_filepath = os.path.join(directory, new_filename)
            if filepath == new_filepath:
                messages.append(f"* {filename} had no changes.")
            else:
                os.rename(filepath, new_filepath)
                messages.append(f"* Renamed {filename} to {new_filename}.")
        else:
            messages.append(f"* {filename} Cannot be renamed.")

    return messages
