import re


class ROMCleaner():

    def clean_rom(self, rom_name: str, delete_trailing_numbers: bool,
                  delete_languages: bool, delete_version: bool, remove_extra_info: bool) -> str:
        # Regardless of the options the app replaces the regions to full name and removes extra info
        rom_name = self.clean_region(rom_name)

        if remove_extra_info:
            rom_name = self.remove_extra_info(rom_name)

        if delete_trailing_numbers:
            rom_name = self.clean_trailing_numbers(rom_name)

        if delete_languages:
            rom_name = self.clean_languages(rom_name)

        if delete_version:
            rom_name = self.clean_version(rom_name)

        # After all of the process remove extra whitespaces and the return the result
        return self.remove_extra_spaces(rom_name)

    def clean_trailing_numbers(self, rom_name: str) -> str:
        # Looks for numbers at the begining that have a hyphen after followed by anything
        return re.sub(r'^\d+\s*-\s*', '', rom_name)

    def clean_languages(self, rom_name: str) -> str:
        # Looks for two letters separated by a space or comma inside parenthesis
        return re.sub(r'\((?:[A-Za-z]{2}(?:\s*[, ]\s*[A-Za-z]{2})*\s*)\)', '', rom_name)

    def clean_version(self, rom_name: str) -> str:
        # Looks for v|rev|version|revision followed by a dot or white space and then numbers inside, all inside a parenthesis
        return re.sub(r'\((v|rev|version|revision)[\s.]*\d+(\.\d+)?\)', '', rom_name, flags=re.IGNORECASE)

    def clean_region(self, rom_name: str) -> str:
        region_mapping = {
            "U": "USA",
            "E": "Europe",
            "J": "Japan",
            "G": "Germany",
            "A": "Australia",
            "C": "China",
            "F": "France",
            "K": "Korea",
            "S": "Spain",
            "T": "Taiwan",
            "B": "Brazil",
            "I": "Italy",
            "H": "Holland"
        }

        def replace(match):
            regions = match.group(1)
            # Check if the matched region code is already a full region name
            if len(regions) > 1 and regions in region_mapping.values():
                return f"({regions})"
            else:
                # The replacement gets concatenated with and comma and white space
                region_names = ', '.join(
                    region_mapping[region] for region in regions)
                return f"({region_names})"

        # Returns the regions in full name, ex: U -> Usa J -> Japan
        return re.sub(r'\(([UEJGACFKSTBIH]+)\)', replace, rom_name)

    def remove_extra_info(self, text: str) -> str:
        patterns_to_preserve = [
            r'\((v|rev|version|revision)[\s.]*\d+(\.\d+)?\)',
            r'\((?:[A-Za-z]{2}(?:\s*[, ]\s*[A-Za-z]{2})*\s*)\)',
            r'\(((USA|Japan|Taiwan|Spain|France|Europe|China|Korea|Germany|Australia|Brazil|Italy|Holland|Finland|Sweden|Netherlands|England|Canada|Greece|Hong Kong|World|Unlicensed)(?:\s*,\s*|\s+)?)+\)'
        ]

        # Join the patterns with "|" for OR operation
        preservation_pattern = '|'.join(patterns_to_preserve)

        # Regular expression pattern to match anything inside brackets
        pattern = r'\([^)]*\)'

        # Compile the regex pattern with IGNORECASE flag needed for the first pattern
        regex = re.compile(
            f'({preservation_pattern})|{pattern}', flags=re.IGNORECASE)

        def replace(match):
            # If the match is one of the preservation patterns, return it as is
            if match.group(1):
                return match.group(1)
            # Otherwise, replace the entire match with an empty string
            else:
                return ''

        # Apply the regex substitution
        pre_result = regex.sub(replace, text)
        return re.sub(r'\[.*?\]', '', pre_result)


    def remove_extra_spaces(self, text: str) -> str:
        # Remove extra spaces
        text = ' '.join(text.split())
        # Remove whitespace before the final period in a filename
        return re.sub(r'\s+\.(?=[^.]*$)', '.', text)
