def map_options(input_list: list[int]) -> tuple[bool, bool, bool, bool]:
    delete_trailing_numbers = 1 in input_list
    delete_languages = 2 in input_list
    delete_version = 3 in input_list
    delete_extra_info = 4 in input_list

    return delete_trailing_numbers, delete_languages, delete_version, delete_extra_info
