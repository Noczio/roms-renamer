import os
from flask_wtf import FlaskForm
from wtforms import StringField, SelectMultipleField
from wtforms import widgets
from error.exception import PathIsEmptyException, PathDoesNotExistException, PathContainsNothingException


class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(html_tag='ol', prefix_label=False)
    option_widget = widgets.CheckboxInput()


class PathForm(FlaskForm):
    options = MultiCheckboxField('Options', choices=[(1, 'Delete trailing numbers'), (
        2, 'Delete languages'), (3, 'Delete version'), (4, 'Delete extra info')], validators=[], coerce=int)

    folder_path = StringField('Folder path', validators=[])

    def validate(self):
        """
        if len(self.options.data) == 0:
            raise MustChooseOneException(
                'At least one option must be selected.')
        """

        if self.folder_path.data.strip() == '':
            raise PathIsEmptyException(
                'Path cannot be empty or consist only of whitespace characters.')

        if not os.path.exists(self.folder_path.data):
            raise PathDoesNotExistException('Path does not exist.')

        if not os.listdir(self.folder_path.data):
            raise PathContainsNothingException('Path contains nothing.')
